﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSimpleFerma
{
    class Program
    {
        
        static void Main(string[] args)
        {

            int p;

            bool f = true;

            Console.WriteLine("Ввести число");

            string v = Console.ReadLine();

            p = Convert.ToInt32(v);


            Random rnd = new Random();

            if ( p == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Введите другое число");
                Console.ReadKey();
                CloseProgram();
            }

            int a = rnd.Next(1, p);


            
            for (int i = 0; i < 1000; i++)
            {

                if (Power(a, p - 1, p) == 1)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                }else
                {
                    f = false;
                    Console.ForegroundColor = ConsoleColor.Red;
                }
                if (i % 5 == 0)
                {
                    Console.WriteLine();
                }


                Console.Write("|       "+Power(a, p - 1, p) + "      |");
                
                    a = rnd.Next(1, p);
                
            }

            Console.WriteLine();
            Console.WriteLine();
            if (f==true)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Простое ");
            }else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Составное");
            }
            Console.ReadKey();

        }


        static long Power(long x, long n, long modules)
        {

            if (modules == 1)
            {
                return (0);
            }


            long result = 1;

            x = x % modules;

            while (n > 0)
            {
                if ((n % 2) == 1)
                {
                    result = (result * x) % modules;
                }
                n = n >> 1;
                x = (x * x) % modules;


            }
            return result;

        }

        public static void CloseProgram()
        {
            Process.GetCurrentProcess().Kill();
        }

    }
}
